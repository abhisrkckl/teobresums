/**
 * Copyright (C) 2017 Alessandro Nagar, Gregorio Carullo, Ka Wa Tsang, Philipp Fleig, Sebastiano Bernuzzi, Walter Del Pozzo
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */

#include <ios>
#include <cmath>
#include <math.h>
#include <vector>
#include <limits>
#include <stdio.h>
#include <fstream>
#include <iostream>
//#include <string.h> 
#include <cstring> 

#include <gsl/gsl_sf.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_complex.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_complex_math.h>

#include "TEOBResumS.h"


using namespace::std;

double Eulerlog(const double x,const double m)

/* Eulerlog function introduced in Eq.(36) of 
   Damour, Iyer & Nagar, PRD 79, 064004 (2009) */
  
{
    
    const double EulerGamma = 0.5772156649015328606065121;
    const double Log2       = 0.6931471805599453094172321;
    
    return EulerGamma + Log2 + log(m) + 0.5*log(x);
}

double interpolate(double dt,vector<gsl_complex> grid)
{
    double xi, yi;
    double x[]      = {0.,0.,0.,0.,0.,0.,0.};
    double y[]      = {0.,0.,0.,0.,0.,0.,0.};
    double step     = 0.01; //dt/5.;
    double omeg_max = 0.;
    double t_max    = 0.;
    bool peak_flag  = false;
    
    for (int i=0; i<=6; i++)
    {
        x[i] = grid[i].dat[0]; //time
        y[i] = grid[i].dat[1]; //omega
    }
    
    
    
    gsl_interp_accel *acc = gsl_interp_accel_alloc ();
    gsl_spline *spline    = gsl_spline_alloc (gsl_interp_cspline, 7);
    gsl_spline_init (spline, x, y, 7);
    
    for (xi = x[0]; xi < x[6]; xi += step)
    {
        yi = gsl_spline_eval (spline, xi, acc);
        if (peak_flag==false)
        {
            if (yi<omeg_max)
            {
                peak_flag=true;
            }
            else
            {
                omeg_max=yi;
                t_max=xi;
            }
        }
    }
    
    gsl_spline_free (spline);
    gsl_interp_accel_free (acc);
    
    return t_max;
}

vector<double> interp_grid(vector<double> t_vec, vector<double> data, double dt)
{
    int i = 0;
    int t_length = t_vec.size();
    int grid_length = (int)((t_vec.back()-t_vec[0])/dt + 1);
    
    double xi, yi;
    vector<double> data_g(grid_length);
    vector<double> omg_interp(grid_length);
    
    /** Convert all vectors to an array */
    double* t = &t_vec[0];
    double* data_arr = &data[0];
    double step = dt;
    
    
    gsl_interp_accel *acc = gsl_interp_accel_alloc ();
    gsl_spline *spline = gsl_spline_alloc (gsl_interp_cspline, t_length);
    gsl_spline_init (spline, t, data_arr, t_length);
    
    for (xi = t_vec[0]; xi < t_vec.back(); xi += step)
    {
        yi = gsl_spline_eval (spline, xi, acc);
        data_g[i] = yi;
        i++;
    }
    
    gsl_spline_free (spline);
    gsl_interp_accel_free (acc);
    
    return data_g;
}



/* Find nearest point index in 1d array */
int find_point_bisection(double x, int n, double *xp, int o)
{
  int i0 = o-1, i1 = n-o;
  int i;

  if (n < 2*o)
  {
    printf(" not enough point to interpolate");
    exit(1);
  }
  
  if (x <= xp[i0]) return 0;
  if (x >  xp[i1]) return n-2*o;

  while (i0 != i1-1) {
    i = (i0+i1)/2;
    if (x < xp[i]) i1 = i; else i0 = i;
  }

  return i0-o+1;
}

/* Barycentric Lagrange interpolation at xx with n points of f(x), 
   equivalent to standard Lagrangian interpolation */   
#define tiny 1e-12
double baryc_f(double xx, int n, double *f, double *x)
{
  
  double omega[n];
  double o, num, den, div, ci;

  int i, j;

  for (i = 0; i < n; i++) {
    
    if (fabs(xx - x[i]) <= tiny) return f[i];

    o = 1.;
    for (j = 0; j < n; j++) {
      if (j != i) {
	o /= (x[i] - x[j]);
      }
    }
    omega[i] = o;
  
  }

  num = den = 0.;
  for (i = 0; i < n; i++) {

    div  = xx - x[i];
    ci   = omega[i]/div;
    den += ci;
    num += ci * f[i];

  }

  return( num/den );
}

/* Barycentric Lagrange interpolation at xx with n points of f(x), 
   compute weights */
void baryc_weights(int n, double *x, double *omega)
{  
  double o;
  int i, j;

  for (i = 0; i < n; i++) {
    
    o = 1.;
    for (j = 0; j < n; j++) {
      if (j != i) { 
	o /= (x[i] - x[j]);
      }
    }
    omega[i] = o;
  
  }

}

/* Barycentric Lagrange interpolation at xx with n points of f(x), 
   use precomputed weights */
double baryc_f_weights(double xx, int n, double *f, double *x, double *omega)
{

  int i;
  double num, den, div, ci;
  
  num = den = 0.;
  for (i = 0; i < n; i++) {

    div  = xx - x[i];
    if (fabs(div) <= tiny) return f[i];

    ci   = omega[i]/div;
    den += ci;
    num += ci * f[i];

  }  

  return( num/den );
}

/* 1d Lagrangian barycentric interpolation */
double interp1d (const int order, double xx, int nx, double *f, double *x)
{
  double ff;
  int ix;
  int ox = order > nx ? nx : order;
  ix = find_point_bisection(xx, nx, x, ox/2);
  ff = baryc_f(xx, ox, &f[ix], &x[ix]);  
  return( ff );
}


vector<gsl_complex> speedyTail(const double Omega, const double Hreal, const double bphys, const int L[], const int M[])
/* This function provides an effective fit of the phase of the Gamma function that enters the EOB resummed
   waveform. The details of this implementation, as well as the definitions used, are given in the Appendix
   of the TEOBResumS, Nagar et al. 2018, in preparation. 
   This functions outputs a vector of 35 components, each component being the residual phase belonging to the
   (l,m) multipole from l=2,...,8 to m=1,...,l*/
{
    int kmax = 35;
    
    double ratio_rad;
    double ratio_ang;
    double tlm_rad;
    double tlm_ang;
    
    double x;
    double x2;
    double x3;
    double x4;
    double x5;
    
    vector<double> num_ang(kmax);
    vector<gsl_complex> tlm(kmax);
    
    /** Fit coefficients*/
    /*
     double a1[] =
     {
     0.3060234167900050, 0.3060234167900050, 0.0424759238428813, 0.0424489015884926, 0.0424717446800903, 0.0215953972500844, 0.0215873812155663, 0.0215776183122621, 0.0216017621863542, 0.0128123696874894, 0.0128097056242375, 0.0128038943888768, 0.0128025242617949, 0.0128202485907368, 0.0083762045692408, 0.0083751913886140, 0.0083724067460769, 0.0083694435961860, 0.0083710364141552, 0.0083834483913443, 0.0058540393221396, 0.0058536069384738, 0.0058522594457692, 0.0058502436535615, 0.0058491157293566, 0.0058514875071582, 0.0058602498033381, 0.0042956812356573, 0.0042954784390887, 0.0042947951664056, 0.0042935886137697, 0.0042923691461384, 0.0042922256848799, 0.0042945927126022, 0.0043009106861259};
     
     double a2[] =
     {
     -0.0248640052699995, -0.0248640052699995, 0.0000597134489198, 0.0002551406918111, 0.0001741036904709, 0.0000124649041611, 0.0000685496215625, 0.0001131160409390, 0.0000419907542591, 0.0000035218982282, 0.0000219211271097, 0.0000473186962874, 0.0000524142634057, 0.0000106823372552, 0.0000012237574387, 0.0000081742188269, 0.0000201940563214, 0.0000295722761753, 0.0000260539631956, 0.0000018994753518, 0.0000004932942990, 0.0000034477210351, 0.0000092294406360, 0.0000155143073237, 0.0000183386499818, 0.0000137922469695, -0.0000007075155453, 0.0000002223410995, 0.0000016045317657, 0.0000045260028113, 0.0000082655700107, 0.0000112393599417, 0.0000115758243113, 0.0000076838709956, -0.0000014020591745
     };
     
     double a3[] = {
     0.1947531537583291, 0.1947531537583291, -0.0042911639711832, -0.0047431560217121, -0.0046577314472149, -0.0013089557502947, -0.0014343968205390, -0.0014978542575474, -0.0014329302934532, -0.0005167994164556, -0.0005573939123058, -0.0005921030407223, -0.0005978284714483, -0.0005673965369076, -0.0002409269302708, -0.0002561516055118, -0.0002723768586352, -0.0002815958312453, -0.0002792078156272, -0.0002646630240693, -0.0001261183503407, -0.0001325622938779, -0.0001403198638518, -0.0001464084186977, -0.0001485971591029, -0.0001459023931717, -0.0001384829633836, -0.0000719062974278, -0.0000749128468013, -0.0000788187384314, -0.0000824202283094, -0.0000846673495936, -0.0000849054394951, -0.0000829269749240, -0.0000788883333858
     };
     
     double a4[] = {
     -0.0254800838696074, -0.0254800838696074, 0.0006914295465364, 0.0010322294603561, 0.0010057563135650, 0.0001394203795507, 0.0002309706405978, 0.0002596611624417, 0.0002409588083156, 0.0000386949167221, 0.0000679154947896, 0.0000830199015202, 0.0000850120755064, 0.0000780125513602, 0.0000133034384660, 0.0000241813441339, 0.0000311573885555, 0.0000340233089866, 0.0000335167900637, 0.0000307571022927, 0.0000053305073331, 0.0000099143129290, 0.0000132296989826, 0.0000150959309402, 0.0000156304390748, 0.0000151274875147, 0.0000139320508803, 0.0000023959090314, 0.0000045285807761, 0.0000061918979830, 0.0000072894226381, 0.0000078251853305, 0.0000078772667984, 0.0000075606242809, 0.0000069956215270
     };
     
     double a5[] = {
     -0.0000140714300659, -0.0000140714300659, 0.0006914295465364, 0.0010322294603561, 0.0010057563135650, 0.0001394203795507, 0.0002309706405978, 0.0002596611624417, 0.0002409588083156, 0.0000386949167221, 0.0000679154947896, 0.0000830199015202, 0.0000850120755064, 0.0000780125513602, 0.0000133034384660, 0.0000241813441339, 0.0000311573885555, 0.0000340233089866, 0.0000335167900637, 0.0000307571022927, 0.0000053305073331, 0.0000099143129290, 0.0000132296989826, 0.0000150959309402, 0.0000156304390748, 0.0000151274875147, 0.0000139320508803, 0.0000023959090314, 0.0000045285807761, 0.0000061918979830, 0.0000072894226381, 0.0000078251853305, 0.0000078772667984, 0.0000075606242809, 0.0000069956215270
     };
     */
    
    double b1[] =
    {
        0.1113090643348557, 0.1112593821157397, 0.0424759238428813, 0.0424489015884926, 0.0424717446800903, 0.0215953972500844, 0.0215873812155663, 0.0215776183122621, 0.0216017621863542, 0.0128123696874894, 0.0128097056242375, 0.0128038943888768, 0.0128025242617949, 0.0128202485907368, 0.0083762045692408, 0.0083751913886140, 0.0083724067460769, 0.0083694435961860, 0.0083710364141552, 0.0083834483913443, 0.0058540393221396, 0.0058536069384738, 0.0058522594457692, 0.0058502436535615, 0.0058491157293566, 0.0058514875071582, 0.0058602498033381, 0.0042956812356573, 0.0042954784390887, 0.0042947951664056, 0.0042935886137697, 0.0042923691461384, 0.0042922256848799, 0.0042945927126022, 0.0043009106861259};
    
    double b2[] =
    {
        0.0004643273300862, 0.0009375605440004, 0.0000597134489198, 0.0002551406918111, 0.0001741036904709, 0.0000124649041611, 0.0000685496215625, 0.0001131160409390, 0.0000419907542591, 0.0000035218982282, 0.0000219211271097, 0.0000473186962874, 0.0000524142634057, 0.0000106823372552, 0.0000012237574387, 0.0000081742188269, 0.0000201940563214, 0.0000295722761753, 0.0000260539631956, 0.0000018994753518, 0.0000004932942990, 0.0000034477210351, 0.0000092294406360, 0.0000155143073237, 0.0000183386499818, 0.0000137922469695, -0.0000007075155453, 0.0000002223410995, 0.0000016045317657, 0.0000045260028113, 0.0000082655700107, 0.0000112393599417, 0.0000115758243113, 0.0000076838709956, -0.0000014020591745
    };
    
    double b3[] =
    {
        -0.0221835462237291, -0.0235386333304348, -0.0042911639711832, -0.0047431560217121, -0.0046577314472149, -0.0013089557502947, -0.0014343968205390, -0.0014978542575474, -0.0014329302934532, -0.0005167994164556, -0.0005573939123058, -0.0005921030407223, -0.0005978284714483, -0.0005673965369076, -0.0002409269302708, -0.0002561516055118, -0.0002723768586352, -0.0002815958312453, -0.0002792078156272, -0.0002646630240693, -0.0001261183503407, -0.0001325622938779, -0.0001403198638518, -0.0001464084186977, -0.0001485971591029, -0.0001459023931717, -0.0001384829633836, -0.0000719062974278, -0.0000749128468013, -0.0000788187384314, -0.0000824202283094, -0.0000846673495936, -0.0000849054394951, -0.0000829269749240, -0.0000788883333858
    };
    
    double b4[] =
    {
        0.0058366730167965, 0.0070452306758401, 0.0006914295465364, 0.0010322294603561, 0.0010057563135650, 0.0001394203795507, 0.0002309706405978, 0.0002596611624417, 0.0002409588083156, 0.0000386949167221, 0.0000679154947896, 0.0000830199015202, 0.0000850120755064, 0.0000780125513602, 0.0000133034384660, 0.0000241813441339, 0.0000311573885555, 0.0000340233089866, 0.0000335167900637, 0.0000307571022927, 0.0000053305073331, 0.0000099143129290, 0.0000132296989826, 0.0000150959309402, 0.0000156304390748, 0.0000151274875147, 0.0000139320508803, 0.0000023959090314, 0.0000045285807761, 0.0000061918979830, 0.0000072894226381, 0.0000078251853305, 0.0000078772667984, 0.0000075606242809, 0.0000069956215270
    };
    
    
    
    const vector<double> Tlm_real = Tlm(Omega*Hreal);
    
    /** Pre-computed psi */
    double psi[] = {0.9227843350984671394, 0.9227843350984671394,
        1.256117668431800473, 1.256117668431800473, 1.256117668431800473,
        1.506117668431800473, 1.506117668431800473, 1.506117668431800473, 1.506117668431800473,
        1.706117668431800473, 1.706117668431800473, 1.706117668431800473, 1.706117668431800473, 1.706117668431800473,
        1.872784335098467139, 1.872784335098467139, 1.872784335098467139, 1.872784335098467139, 1.872784335098467139, 1.872784335098467139,
        2.015641477955609997, 2.015641477955609997, 2.015641477955609997, 2.015641477955609997, 2.015641477955609997, 2.015641477955609997, 2.015641477955609997,
        2.140641477955609997, 2.140641477955609997, 2.140641477955609997, 2.140641477955609997, 2.140641477955609997, 2.140641477955609997, 2.140641477955609997, 2.140641477955609997};
    
    double k;
    for (int i=kmax; i--;)
    {
        k  = M[i] * Omega;
        x  = k * Hreal; //hathatk
        x2 = x * x;
        x3 = x2 * x;
        x4 = x3 * x;
        x5 = x4 * x;
        // Why no linear term in the current implementation?
        
        //num_ang[i] = (1. + a1[i]*x2 + a2[i]*x3)/(1. + a3[i]*x2 + a4[i]*x3 + a5[i]*x4); //A.N. fit
        num_ang[i]    = 1. + b1[i]*x2 + b2[i]*x3 + b3[i]*x4 + b4[i]*x5; //P.F. fit
        
        ratio_rad     = Tlm_real[i];
        ratio_ang     = - 2. * psi[i] * x * num_ang[i];
        tlm_rad       = ratio_rad;
        tlm_ang       = ratio_ang + 2. * x * log(2. * k * bphys);
        
        tlm[i].dat[0] = tlm_rad;
        tlm[i].dat[1] = tlm_ang;
    }
    
    return tlm;
}

/* factorial */
double fact(int n)
{
    double f[] = {1., 1., 2., 6., 24., 120., 720., 5040., 40320., 362880.,
        3628800., 39916800., 479001600., 6227020800., 87178291200.};
    if (n < 0){
        fprintf(stderr, " computing a negative factorial. \n");
        exit(1);
    } else if (n <= 14){
        return f[n];
    } else {
        return n*fact(n-1);
    }
}

/* Wigner d-function */
/* This corresponds to Eq. (II.8) of arXiv:0709.0093 */
double wigner_d_function(int l, int m, int s, double i)
{
    double dWig = 0.;
    
    double costheta = cos(i*0.5);
    double sintheta = sin(i*0.5);
    
    int ki = std::max( 0  , m-s );
    int kf = std::min( l+m, l-s );
    
    for( int k = ki; k <= kf; k++ ){
        dWig +=
        ( pow(-1.,k) * pow(costheta,2*l+m-s-2*k) * pow(sintheta,2*k+s-m) )/
        ( fact(k) * fact(l+m-k) * fact(l-s-k) * fact(s-m+k) );
    }
    
    return (sqrt(fact(l+m) * fact(l-m) * fact(l+s) * fact(l-s)) * dWig);
}

/* spin-weighted spherical harmonic */
void spinsphericalharm(double *rY, double *iY, int s, int l, int m, double phi, double i)
{
    /* Following the Ref.: https://arxiv.org/pdf/0709.0093.pdf */
    if ((l<0) || (m<-l) || (m>l)) {
        fprintf(stderr, " wrong l (%d) or m (%d) inside spinspharmY\n", l, m);
        exit(1);
    }
    
    double c = pow(-1.,-s) * sqrt( (2.*l+1.)/(4.*M_PI) );
    double dWigner = c * wigner_d_function(l,m,-s,i);
    
    *rY = cos((double)(m)*phi) * dWigner;
    *iY = sin((double)(m)*phi) * dWigner;
}

double fLR(double r, void *params)
{
    /** Computing the light ring */

    vector<double> metric=Metric(r, params,true);
    
    double A  = metric[0];
    double dA = metric[2]; /**derivative w.r.t. u*/
    double f  = A +(0.5*dA)/r;
    
    return f;
}
/** Takes nu as input */
double AdiabLR(void *params)
/* This function computes the adiabatic light-ring. This is used
   as input in the GSF-modified tidal potential, see PRL 114 (2015) 161103
   and references therein as reference */
{
    
    int status;
    int iter = 0, max_iter = 200;
    const gsl_root_fsolver_type *T;
    double rLR;
    double x_lo = 0.1, x_hi = 15.;
    
    gsl_root_fsolver *s;
    gsl_function F;
    F.function = &fLR;
    F.params = params;
    T = gsl_root_fsolver_bisection;
    s = gsl_root_fsolver_alloc (T);
    gsl_root_fsolver_set (s, &F, x_lo, x_hi);
    
    do
    {
        iter++;
        status = gsl_root_fsolver_iterate (s);
        rLR    = gsl_root_fsolver_root (s);
        x_lo   = gsl_root_fsolver_x_lower (s);
        x_hi   = gsl_root_fsolver_x_upper (s);
        status = gsl_root_test_interval (x_lo, x_hi, 0, 0.000000000000001);
    }
    while (status == GSL_CONTINUE && iter < max_iter);
    gsl_root_fsolver_free (s);
    
    return rLR;
}

vector<double> s_D1(vector<double> f, vector<double> x, int Nmax)
{
    /* Computes the numerical derivative with a 4th-order stancil.
       Boundaries are correctly implemented at 4th order. */
    int Nmin = 0;
    
    vector<double> df(Nmax+1);
    for(int i=2;i<=Nmax-2;i++)
    {
        df[i] = 1./3.*(8.*f[1+i] - f[2+i] - 8.*f[i-1] + f[i-2])/(x[2+i]-x[i-2]);
    }
    
    // 4th order boundaries - from Wolfram http://reference.wolfram.com/language/tutorial/NDSolveMethodOfLines.html
    df[0]      = (-25*f[Nmin] + 48*f[Nmin+1] - 36*f[Nmin+2] +16*f[Nmin+3]-3*f[Nmin+4])/(3*(x[Nmin+4]-x[Nmin]));
    df[1]      = (-3*f[Nmin]-10*f[Nmin+1]+18*f[Nmin+2]-6*f[Nmin+3]+f[Nmin+4])/(3*(x[Nmin+4]-x[Nmin]));
    df[Nmax]   = (25*f[Nmax] - 48*f[Nmax-1] + 36*f[Nmax-2] - 16*f[Nmax-3]+3*f[Nmax-4])/(3*(x[Nmax]-x[Nmax-4]));
    df[Nmax-1] = (-f[Nmax-4]+6*f[Nmax-3]-18*f[Nmax-2]+10*f[Nmax-1]+3*f[Nmax])/(3*(x[Nmax]-x[Nmax-4])); 
    
    return df;
}

/** Sets the dynamics controlling flags to their default value */
void SetDefaultFlagsValues(TEOBResumFlags *flags)
{
    flags->spin             = 0;
    flags->tidal            = 0;
    flags->RWZ              = 0;
    flags->speedy           = 1;
    flags->dynamics         = 0;
    flags->multipoles       = 0;
    flags->Yagi_fits        = 0;
    flags->geometric_units  = 0;
    flags->solver_scheme    = 0;
    flags->set              = 0;
}

double logQ(double x)
{
    /** This function computes the quadrupole induced by rotation. Reference: Yunes-Yagi, PRD 88, 023009.logQ-vs-log(lambda) fit of Table I of Yunes-Yagi
     here x = log(lambda) and the output is the log of the coefficient
     that describes the quadrupole deformation due to spin. */
    double ai = 0.194;
    double bi = 0.0936;
    double ci = 0.0474;
    double di = -4.21e-3;
    double ei = 1.23e-4;
    double x2 = x*x;
    double x3 = x*x2;
    double x4 = x*x3;
    
    return ai + bi*x + ci*x2 + di*x3 + ei*x4;
}

double Yagi13_fit_barlamdel(double barlam2, int ell)
{
    /*
     Yagi 2013 fits for multipolar
     $\bar{\lambda}_\ell$ = 2 k_\ell/(C^{2\ell+1} (2\ell-1)!!)$
     Eq.(10),(61); Tab.I; Fig.8 http://arxiv.org/abs/1311.0872
     Only the coefficients for NS are used.
     “lny” can be identified with Eq. (61).
     */
    double lnx = log(barlam2);
    double coeffs[5] = {0.0};
    if (ell == 3)
    {
        coeffs[0] = 2.52e-5;
        coeffs[1] = -1.31e-3;
        coeffs[2] = 2.51e-2;
        coeffs[3] = 1.18;
        coeffs[4] = -1.15;
    }
    else if (ell == 4)
    {
        coeffs[0] = 2.8e-5;
        coeffs[1] = -1.81e-3;
        coeffs[2] = 3.95e-2;
        coeffs[3] = 1.43;
        coeffs[4] = -2.45;
    }
    else return 0.0;
    
    double lny = coeffs[0]*lnx*lnx*lnx*lnx+coeffs[1]*lnx*lnx*lnx+coeffs[2]*lnx*lnx+coeffs[3]*lnx+coeffs[4];
    return exp(lny);
}

double radius0(double M, double f_start,double chi1,double chi2)
/* This function computed the initial radius r0 to feed the
   EOB dynamics from a given value f_start in Hz. This is 
   done using the Newtonian Kepler's law */
  
{
	if (chi1<0 || chi2<0)
	{
	    /* Quick fix to avoid f_min (by default 20 or 10Hz) to be too 
	       high when the total mass is large, e.g. for binaries 
	       of 100 + 100. If the default f_min is kept the dynamics
	       in this case starts in the plunge already and the code gives
	       and error that just comes from the choice of the initial radius.
	       Here we arbitrarily decide to increase the initial frequency
	       to be sure to have enough cycles of inspiral using starting
	       from the Schwarzschild frequency corresponding to r=8.
	       This is a simple way to avoid the problem. In the future one
	       will have to do this by monitoring the EOB LSO frequency, 
	       that always exists when the spins are anti-aligned with the
	       orbital angular momentum.

	       Note also that the intial radius r_0 is computed from the frequency
	       using the Newtonian Kepler's law, that is just an approximation in
	       the EOB case. This means that, even if one inputs a value f_start,
	       the intial starting frequency of the waveform is actually different.

	       This is of no importance here: one should just make sure that there
	       are enough inspiral cycles in the waveform so to capture the full
	       transition from inspiral to plunge and avoid evident inaccuracies.*/ 
    
		double fcirc_Schw = 0.04419417382415922/(M*MSUN_S*M_PI);
		if (f_start >=fcirc_Schw)
		{
			f_start = 0.5*fcirc_Schw;
		}

	}


  
    double x = (M*f_start*MSUN_S*2.*M_PI)/2.;
    return cbrt(1/(x*x));
}

double time_units_conversion(double M, double dt)
{
    return dt/(M*MSUN_S);
}

TEOBResumParams read_config(char *fname)
{

    TEOBResumParams params;
    
    // set defaults (flags & pars)
    SetDefaultFlagsValues(&params.flags);
    
    params.q         = 1.;       // mass ratio m1/m2
    params.mtot      = 80.;      // total mass [Msun]
    params.chi1      = 0.;       // dimensionless spin body 1
    params.chi2      = 0.;       // dimensionless spin body 2
    params.dt        = 1./4096.; // temporal spacing [s]
    params.f_min     = 10;       // initial frequency [Hz]
    params.lm        = 1;        // multipolar index
    params.LambdaAl2 = 0.;       // l=2 tidal polarizability (A=1)
    params.LambdaAl3 = 0.;       // l=3 tidal polarizability (A=1)
    params.LambdaAl4 = 0.;       // l=4 tidal polarizability (A=1)
    params.LambdaBl2 = 0.;       // l=2 tidal polarizability (B=2)
    params.LambdaBl3 = 0.;       // l=3 tidal polarizability (B=2)
    params.LambdaBl4 = 0.;       // l=4 tidal polarizability (B=2)
    params.distance  = 100;      // Mpc
    params.iota      = 0.0;
    params.coa_phase = 0.0;

    string param_name;
    double param_value;
    
    ifstream fin (fname);
    if (!fin) //checks to see if file opens properly
    {
        cerr << "error: Could not find the parameters file.";
    }
    
    int i = 0;
    
    while ( fin >> param_name >> param_value )
    {
        //param_values.push_back(param_value);
        cout << param_name <<"\t"<< param_value << endl;

	// pars
	if(param_name=="Mtot") {
	  params.mtot = param_value;
	}
	if(param_name=="iota") {
	  params.iota = param_value;
	}
	if(param_name=="coa_phase") {
	  params.coa_phase = param_value;
	}
	if(param_name=="distance") {
	  params.distance = param_value;
	}
	if(param_name=="q") {
	  params.q = param_value;
	}
	if(param_name=="chi1") {
	  params.chi1 = param_value;
	}
	if(param_name=="chi2") {
	  params.chi2 = param_value;
	}
	if(param_name=="f_min") {
	  params.f_min = param_value;
	}
	if(param_name=="lm") {
	  params.lm = param_value;
	}
	if(param_name=="dt") {
	  params.dt = param_value;
	}
	if(param_name=="LambdaAl2") {
	  params.LambdaAl2 = param_value;
	}
	if(param_name=="LambdaBl2") {
	  params.LambdaBl2 = param_value;
	}
	if(param_name=="LambdaAl3") {
	  params.LambdaAl3 = param_value;
	}
	if(param_name=="LambdaBl3") {
	  params.LambdaBl3 = param_value;
	}
	if(param_name=="LambdaAl4") {
	  params.LambdaAl4 = param_value;
	}
	if(param_name=="LambdaBl4") {
	  params.LambdaBl4 = param_value;
	}

	// flags
	if(param_name=="tidal") {
	  params.flags.tidal = param_value;
	}
	if(param_name=="RWZ") {
	  params.flags.RWZ = param_value;
	}
	if(param_name=="speedy") {
	  params.flags.speedy = param_value;
	}
	if(param_name=="dynamics") {
	  params.flags.dynamics = param_value;
	}
	if(param_name=="Yagi_fit") {
	  params.flags.Yagi_fits = param_value;
	}
	if(param_name=="multipoles") {
	  params.flags.multipoles = param_value;
	}
	if(param_name=="solver_scheme") {
	  params.flags.solver_scheme = param_value;
	}
	if(param_name=="geometric_units") {
	  params.flags.geometric_units = param_value;
	}

        i++;
    }
    fin.close();
    
    // calculate parameters
    
    double chi1 = params.chi1;
    double chi2 = params.chi2;
    double q = params.q;
    if (params.flags.Yagi_fits == 1)
    {
        params.LambdaAl3 = Yagi13_fit_barlamdel(params.LambdaAl2, 3);
        params.LambdaBl3 = Yagi13_fit_barlamdel(params.LambdaBl2, 3);
        params.LambdaAl4 = Yagi13_fit_barlamdel(params.LambdaAl2, 4);
        params.LambdaBl4 = Yagi13_fit_barlamdel(params.LambdaBl2, 4);
    }
    
    /** Override spin settings if spins are given in input */
    if (chi1 != .0 || chi2 != .0) params.flags.spin = 1;
    
    double nu = q/((q+1.)*(q+1.));
    params.nu = nu;

    /* X_i = m_i/M; X1>X2 */
    double X1 = 0.5*(1.+sqrt(1.-4.*nu));
    double X2 = 1. - X1;
    double XA = X1; // a different notation used in tidal part, keep here for simplicity
    double XB = X2;
    params.X1 = X1;
    params.X2 = X2;
    
    double S1 = params.X1 * params.X1 * params.chi1;
    double S2 = params.X2 * params.X2 * params.chi2;
    params.S1 = S1;
    params.S2 = S2;
    
    double a1  = X1*chi1;  //tilde{a}_1 = S_1/(m_1 M)
    double a2  = X2*chi2;  //tilde{a}_1 = S_2/(m_2 M)
    double aK  = a1 + a2;
    double aK2 = aK*aK;
    
    params.S     = S1 + S2;        // => in the EMRL this becomes the spin of the BH
    params.Sstar = X2*a1 + X1*a2;  // => in the EMRL this becomes the spin of the particle
    
    params.a1  = X1*chi1;
    params.a2  = X2*chi2;
    params.aK  = a1 + a2;
    params.aK2 = aK2;
    params.rLR = 0.;
    /* NNNLO NR-informed effective spin-orbit parameter.
       See Eq. (26) and (27)-(34) of Nagar, Riemenschneider and Pratten,
       PRD 96, 084045 (2017) */
    params.cN3LO = c3_fit_global(nu,chi1,chi2,X1,X2,a1,a2,params.flags.tidal);
    
    
    // tidal params
    double LambdaAl2 = params.LambdaAl2;
    double LambdaAl3 = params.LambdaAl3;
    double LambdaAl4 = params.LambdaAl4;
    
    double LambdaBl2 = params.LambdaBl2;
    double LambdaBl3 = params.LambdaBl3;
    double LambdaBl4 = params.LambdaBl4;
    
    /** Computing the tidal coupling constants */
    
    double kapA2 = 3.   * LambdaAl2 * XA*XA*XA*XA*XA / q; //Note: kap stands for kappa; see eqn(1) of REF
    double kapA3 = 15.  * LambdaAl3 * XA*XA*XA*XA*XA*XA*XA / q;
    double kapA4 = 105. * LambdaAl4 * XA*XA*XA*XA*XA*XA*XA*XA*XA / q;
    
    double kapB2 = 3.   * LambdaBl2 * XB*XB*XB*XB*XB * q;
    double kapB3 = 15.  * LambdaBl3 * XB*XB*XB*XB*XB*XB*XB * q;
    double kapB4 = 105. * LambdaBl4 * XB*XB*XB*XB*XB*XB*XB*XB*XB * q;
    
    double kapT2 = kapA2 + kapB2;
    double kapT3 = kapA3 + kapB3;
    double kapT4 = kapA4 + kapB4;
    
    params.kappaAl2 = kapA2;
    params.kappaAl3 = kapA3;
    params.kappaAl4 = kapA4;
    
    params.kappaBl2 = kapB2;
    params.kappaBl3 = kapB3;
    params.kappaBl4 = kapB4;
    
    params.kappaTl2 = kapT2;
    params.kappaTl3 = kapT3;
    params.kappaTl4 = kapT4;
    
    //-----------------------------------------------------------------------------------
    // Definition of the conservative tidal coefficients \bar{\alpha}_n^{(\ell)}, Eq.(37)
    // of Damour&Nagar, PRD 81, 084016 (2010)
    //-----------------------------------------------------------------------------------
    params.bar_alph2_1 = (5./2.*XA*kapA2 + 5./2.*XB*kapB2)/kapT2;
    params.bar_alph2_2 = ((3.+XA/8.+ 337./28.*XA*XA)*kapA2 + (3.+XB/8.+ 337./28.*XB*XB)*kapB2)/kapT2;/**Eq. 6.10 di Bini, Damour, Faye, PRD 85 124034 */
    params.bar_alph3_1 = ((-2.+15./2.*XA)*kapA3 + (-2.+15./2.*XB)*kapB3)/kapT3;/**Eq. 6.21 di Bini, Damour, Faye, PRD 85 124034 */
    params.bar_alph3_2 = ((8./3.-311./24.*XA+110./3.*XA*XA)*kapA3 + (8./3.-311./24.*XB+110./3.*XB*XB)*kapB3)/kapT3;/**Eq. 6.22 di Bini, Damour, Faye, PRD 85 124034 */
    
    
    //double lambda1 = params.LambdaAl2;
    //double lambda2 = params.LambdaBl2;
            
    double logC_Q1 = logQ(log(LambdaAl2));
    double logC_Q2 = logQ(log(LambdaBl2));
    double C_Q1    = exp(logC_Q1);
    double C_Q2    = exp(logC_Q2);
    params.C_Q1    = C_Q1;
    params.C_Q2    = C_Q2;
        
    return params;
}

void CopyTEOBResumSFlags(TEOBResumFlags *out, TEOBResumFlags *in)
{
    out->spin  = in->spin;
    out->tidal = in->tidal;
    out->RWZ   = in->RWZ;
    out->speedy = in->speedy;
    out->dynamics = in->dynamics;
    out->multipoles = in->multipoles;
    out->Yagi_fits  = in->Yagi_fits;
    out->geometric_units = in->geometric_units;
    out->solver_scheme = in->solver_scheme;
    out->set = in->set;
}

TEOBResumParams process_input_parameters(
                                         double m1,
                                         double m2,
                                         double chi1,
                                         double chi2,
                                         double f_min,
                                         double dt,
                                         double LambdaAl2,
                                         double LambdaBl2,
                                         double LambdaAl3,
                                         double LambdaBl3,
                                         double LambdaAl4,
                                         double LambdaBl4,
                                         TEOBResumFlags *flags   /** flags **/
                                         )
{
    TEOBResumParams params;
    
    CopyTEOBResumSFlags(&params.flags,flags);

    double mtot = m1+m2;
    double q    = m1/m2;
    double nu   = q/((q+1.)*(q+1.));
    params.mtot = mtot;
    params.q    = q;
    params.chi1 = chi1;
    params.chi2 = chi2;
    params.nu   = nu;
    double X1   = 0.5*(1.+sqrt(1.-4.*nu));
    double X2   = 1. - X1;
    
    if (params.flags.geometric_units==0)
    {
      // input given in physical units, 
      // rescale to geometric units and mass rescaled quantities
      // compute r0 from the initial GW frequency in Hz
        params.dt = time_units_conversion(mtot, dt);
        params.r0 = radius0(mtot, f_min,params.chi1,params.chi2);
    }
    else
    {
      // input given in geometric units, 
      // rescale to geometric units and mass rescaled quantities
      // compute r0 from the initial GW frequency in geometric units and mass rescaled
        params.dt = dt;
        params.r0 = pow(f_min*M_PI, -2./3.);
    }
    if (DEBUG)
    {
        printf(" dt = %e r0 = %e\n",params.dt, params.r0);
    }
        
    if (params.flags.tidal == 1 && params.flags.Yagi_fits==1)
    {
        LambdaAl3 = Yagi13_fit_barlamdel(LambdaAl2, 3);
        LambdaBl3 = Yagi13_fit_barlamdel(LambdaBl2, 3);
        LambdaAl4 = Yagi13_fit_barlamdel(LambdaAl2, 4);
        LambdaBl4 = Yagi13_fit_barlamdel(LambdaBl2, 4);
    }
    
    /** Override spin settings if spins are given in input */
    if (chi1 != .0 || chi2 != .0) params.flags.spin = 1;
    
    double XA = X1; // a different notation used in tidal part, keep here for simplicity
    double XB = X2;
    params.X1 = X1;
    params.X2 = X2;
    
    double S1 = params.X1*params.X1 * params.chi1;
    double S2 = params.X2*params.X2 * params.chi2;
    params.S1 = S1;
    params.S2 = S2;
    
    double a1  = X1*chi1;
    double a2  = X2*chi2;
    double aK  = a1 + a2;
    double aK2 = aK*aK;
    
    params.S     = S1 + S2;        // => in the EMRL this becomes the spin of the BH
    params.Sstar = X2*a1 + X1*a2;  // => in the EMRL this becomes the spin of the particle
    
    params.a1  = X1*chi1;
    params.a2  = X2*chi2;
    params.aK  = a1 + a2;
    params.aK2 = aK2;
    
    params.rLR = 0.;
    
    params.cN3LO = c3_fit_global(nu,chi1,chi2,X1,X2,a1,a2,params.flags.tidal);
    
    /** Computing the tidal coupling constants. Defined in ref: https://arxiv.org/pdf/1412.4553.pdf */
    
    double kapA2 = 3.   * LambdaAl2 * XA*XA*XA*XA*XA / q; //Note: kap stands for kappa; see eqn(1) of https://arxiv.org/pdf/1412.4553.pdf
    double kapA3 = 15.  * LambdaAl3 * XA*XA*XA*XA*XA*XA*XA / q;
    double kapA4 = 105. * LambdaAl4 * XA*XA*XA*XA*XA*XA*XA*XA*XA / q;
    
    double kapB2 = 3.   * LambdaBl2 * XB*XB*XB*XB*XB * q;
    double kapB3 = 15.  * LambdaBl3 * XB*XB*XB*XB*XB*XB*XB * q;
    double kapB4 = 105. * LambdaBl4 * XB*XB*XB*XB*XB*XB*XB*XB*XB * q;
    
    double kapT2 = kapA2 + kapB2;
    double kapT3 = kapA3 + kapB3;
    double kapT4 = kapA4 + kapB4;
    
    params.kappaAl2 = kapA2;
    params.kappaAl3 = kapA3;
    params.kappaAl4 = kapA4;
    
    params.kappaBl2 = kapB2;
    params.kappaBl3 = kapB3;
    params.kappaBl4 = kapB4;
    
    params.kappaTl2 = kapT2;
    params.kappaTl3 = kapT3;
    params.kappaTl4 = kapT4;
    
    //-----------------------------------------------------------------------------------
    // Definition of the conservative tidal coefficients \bar{\alpha}_n^{(\ell)}, Eq.(37)
    // of Damour&Nagar, PRD 81, 084016 (2010)
    //-----------------------------------------------------------------------------------
    params.bar_alph2_1 = (5./2.*XA*kapA2 + 5./2.*XB*kapB2)/kapT2;
    params.bar_alph2_2 = ((3.+XA/8.+ 337./28.*XA*XA)*kapA2 + (3.+XB/8.+ 337./28.*XB*XB)*kapB2)/kapT2;
    params.bar_alph3_1 = ((-2.+15./2.*XA)*kapA3 + (-2.+15./2.*XB)*kapB3)/kapT3;			     			   
    params.bar_alph3_2 = ((8./3.-311./24.*XA+110./3.*XA*XA)*kapA3 + (8./3.-311./24.*XB+110./3.*XB*XB)*kapB3)/kapT3;
    
    double logC_Q1 = logQ(log(LambdaAl2));
    double logC_Q2 = logQ(log(LambdaBl2));
    double C_Q1    = exp(logC_Q1);
    double C_Q2    = exp(logC_Q2);    

    params.C_Q1 = C_Q1;
    params.C_Q2 = C_Q2;

    params.LambdaAl2 = LambdaAl2;
    params.LambdaAl3 = LambdaAl3;
    params.LambdaAl4 = LambdaAl4;
    params.LambdaBl2 = LambdaBl2;
    params.LambdaBl3 = LambdaBl3;
    params.LambdaBl4 = LambdaBl4;
    
    
    
    return params;
}


void LALSpinWeightedSphericalHarmonic(double *rY,   /**< real part of the harmonic */
                                      double *iY,   /**< imaginary part of the harmonic */
                                      int s,        /**< spin weight */
                                      int l,        /**< mode number l */
                                      int m,         /**< mode number m */
                                      double theta,  /**< polar angle (rad) */
                                      double phi    /**< azimuthal angle (rad) */
    )
{
  double fac = 0.0;

  if ( s == -2 )
  {
    if ( l == 2 )
    {
      switch ( m )
      {
        case -2:
          fac = sqrt( 5.0 / ( 64.0 * M_PI ) ) * ( 1.0 - cos( theta ))*( 1.0 - cos( theta ));
          break;
        case -1:
          fac = sqrt( 5.0 / ( 16.0 * M_PI ) ) * sin( theta )*( 1.0 - cos( theta ));
          break;

        case 0:
          fac = sqrt( 15.0 / ( 32.0 * M_PI ) ) * sin( theta )*sin( theta );
          break;

        case 1:
          fac = sqrt( 5.0 / ( 16.0 * M_PI ) ) * sin( theta )*( 1.0 + cos( theta ));
          break;

        case 2:
          fac = sqrt( 5.0 / ( 64.0 * M_PI ) ) * ( 1.0 + cos( theta ))*( 1.0 + cos( theta ));
          break;
        default:
          break;
      } /*  switch (m) */
    }  /* l==2*/
    else if ( l == 3 )
    {
      switch ( m )
      {
        case -3:
          fac = sqrt(21.0/(2.0*M_PI))*cos(theta/2.0)*pow(sin(theta/2.0),5.0);
          break;
        case -2:
          fac = sqrt(7.0/(4.0*M_PI))*(2.0 + 3.0*cos(theta))*pow(sin(theta/2.0),4.0);
          break;
        case -1:
          fac = sqrt(35.0/(2.0*M_PI))*(sin(theta) + 4.0*sin(2.0*theta) - 3.0*sin(3.0*theta))/32.0;
          break;
        case 0:
          fac = (sqrt(105.0/(2.0*M_PI))*cos(theta)*pow(sin(theta),2.0))/4.0;
          break;
        case 1:
          fac = -sqrt(35.0/(2.0*M_PI))*(sin(theta) - 4.0*sin(2.0*theta) - 3.0*sin(3.0*theta))/32.0;
          break;

        case 2:
          fac = sqrt(7.0/M_PI)*pow(cos(theta/2.0),4.0)*(-2.0 + 3.0*cos(theta))/2.0;
          break;

        case 3:
          fac = -sqrt(21.0/(2.0*M_PI))*pow(cos(theta/2.0),5.0)*sin(theta/2.0);
          break;

        default:
          break;
      }
    }   /* l==3 */
    else if ( l == 4 )
    {
      switch ( m )
      {
        case -4:
          fac = 3.0*sqrt(7.0/M_PI)*pow(cos(theta/2.0),2.0)*pow(sin(theta/2.0),6.0);
          break;
        case -3:
          fac = 3.0*sqrt(7.0/(2.0*M_PI))*cos(theta/2.0)*(1.0 + 2.0*cos(theta))*pow(sin(theta/2.0),5.0);
          break;

        case -2:
          fac = (3.0*(9.0 + 14.0*cos(theta) + 7.0*cos(2.0*theta))*pow(sin(theta/2.0),4.0))/(4.0*sqrt(M_PI));
          break;
        case -1:
          fac = (3.0*(3.0*sin(theta) + 2.0*sin(2.0*theta) + 7.0*sin(3.0*theta) - 7.0*sin(4.0*theta)))/(32.0*sqrt(2.0*M_PI));
          break;
        case 0:
          fac = (3.0*sqrt(5.0/(2.0*M_PI))*(5.0 + 7.0*cos(2.0*theta))*pow(sin(theta),2.0))/16.0;
          break;
        case 1:
          fac = (3.0*(3.0*sin(theta) - 2.0*sin(2.0*theta) + 7.0*sin(3.0*theta) + 7.0*sin(4.0*theta)))/(32.0*sqrt(2.0*M_PI));
          break;
        case 2:
          fac = (3.0*pow(cos(theta/2.0),4.0)*(9.0 - 14.0*cos(theta) + 7.0*cos(2.0*theta)))/(4.0*sqrt(M_PI));
          break;
        case 3:
          fac = -3.0*sqrt(7.0/(2.0*M_PI))*pow(cos(theta/2.0),5.0)*(-1.0 + 2.0*cos(theta))*sin(theta/2.0);
          break;
        case 4:
          fac = 3.0*sqrt(7.0/M_PI)*pow(cos(theta/2.0),6.0)*pow(sin(theta/2.0),2.0);
          break;
        default:
          break;
      }
    }    /* l==4 */
    else if ( l == 5 )
    {
      switch ( m )
      {
        case -5:
          fac = sqrt(330.0/M_PI)*pow(cos(theta/2.0),3.0)*pow(sin(theta/2.0),7.0);
          break;
        case -4:
          fac = sqrt(33.0/M_PI)*pow(cos(theta/2.0),2.0)*(2.0 + 5.0*cos(theta))*pow(sin(theta/2.0),6.0);
          break;
        case -3:
          fac = (sqrt(33.0/(2.0*M_PI))*cos(theta/2.0)*(17.0 + 24.0*cos(theta) + 15.0*cos(2.0*theta))*pow(sin(theta/2.0),5.0))/4.0;
          break;
        case -2:
          fac = (sqrt(11.0/M_PI)*(32.0 + 57.0*cos(theta) + 36.0*cos(2.0*theta) + 15.0*cos(3.0*theta))*pow(sin(theta/2.0),4.0))/8.0;
          break;
        case -1:
          fac = (sqrt(77.0/M_PI)*(2.0*sin(theta) + 8.0*sin(2.0*theta) + 3.0*sin(3.0*theta) + 12.0*sin(4.0*theta) - 15.0*sin(5.0*theta)))/256.0;
          break;
        case 0:
          fac = (sqrt(1155.0/(2.0*M_PI))*(5.0*cos(theta) + 3.0*cos(3.0*theta))*pow(sin(theta),2.0))/32.0;
          break;
        case 1:
          fac = sqrt(77.0/M_PI)*(-2.0*sin(theta) + 8.0*sin(2.0*theta) - 3.0*sin(3.0*theta) + 12.0*sin(4.0*theta) + 15.0*sin(5.0*theta))/256.0;
          break;
        case 2:
          fac = sqrt(11.0/M_PI)*pow(cos(theta/2.0),4.0)*(-32.0 + 57.0*cos(theta) - 36.0*cos(2.0*theta) + 15.0*cos(3.0*theta))/8.0;
          break;
        case 3:
          fac = -sqrt(33.0/(2.0*M_PI))*pow(cos(theta/2.0),5.0)*(17.0 - 24.0*cos(theta) + 15.0*cos(2.0*theta))*sin(theta/2.0)/4.0;
          break;
        case 4:
          fac = sqrt(33.0/M_PI)*pow(cos(theta/2.0),6.0)*(-2.0 + 5.0*cos(theta))*pow(sin(theta/2.0),2.0);
          break;
        case 5:
          fac = -sqrt(330.0/M_PI)*pow(cos(theta/2.0),7.0)*pow(sin(theta/2.0),3.0);
          break;
        default:
          break;
      }
    }  /* l==5 */
    else if ( l == 6 )
    {
      switch ( m )
      {
        case -6:
          fac = (3.*sqrt(715./M_PI)*pow(cos(theta/2.0),4)*pow(sin(theta/2.0),8))/2.0;
          break;
        case -5:
          fac = (sqrt(2145./M_PI)*pow(cos(theta/2.0),3)*(1. + 3.*cos(theta))*pow(sin(theta/2.0),7))/2.0;
          break;
        case -4:
          fac = (sqrt(195./(2.0*M_PI))*pow(cos(theta/2.0),2)*(35. + 44.*cos(theta)
          + 33.*cos(2.*theta))*pow(sin(theta/2.0),6))/8.0;
          break;
        case -3:
          fac = (3.*sqrt(13./M_PI)*cos(theta/2.0)*(98. + 185.*cos(theta) + 110.*cos(2*theta)
          + 55.*cos(3.*theta))*pow(sin(theta/2.0),5))/32.0;
          break;
        case -2:
          fac = (sqrt(13./M_PI)*(1709. + 3096.*cos(theta) + 2340.*cos(2.*theta) + 1320.*cos(3.*theta)
          + 495.*cos(4.*theta))*pow(sin(theta/2.0),4))/256.0;
          break;
        case -1:
          fac = (sqrt(65./(2.0*M_PI))*cos(theta/2.0)*(161. + 252.*cos(theta) + 252.*cos(2.*theta)
          + 132.*cos(3.*theta) + 99.*cos(4.*theta))*pow(sin(theta/2.0),3))/64.0;
          break;
        case 0:
          fac = (sqrt(1365./M_PI)*(35. + 60.*cos(2.*theta) + 33.*cos(4.*theta))*pow(sin(theta),2))/512.0;
          break;
        case 1:
          fac = (sqrt(65./(2.0*M_PI))*pow(cos(theta/2.0),3)*(161. - 252.*cos(theta) + 252.*cos(2.*theta)
          - 132.*cos(3.*theta) + 99.*cos(4.*theta))*sin(theta/2.0))/64.0;
          break;
        case 2:
          fac = (sqrt(13./M_PI)*pow(cos(theta/2.0),4)*(1709. - 3096.*cos(theta) + 2340.*cos(2.*theta)
          - 1320*cos(3*theta) + 495*cos(4*theta)))/256.0;
          break;
        case 3:
          fac = (-3.*sqrt(13./M_PI)*pow(cos(theta/2.0),5)*(-98. + 185.*cos(theta) - 110.*cos(2*theta)
          + 55.*cos(3.*theta))*sin(theta/2.0))/32.0;
          break;
        case 4:
          fac = (sqrt(195./(2.0*M_PI))*pow(cos(theta/2.0),6)*(35. - 44.*cos(theta)
          + 33.*cos(2*theta))*pow(sin(theta/2.0),2))/8.0;
          break;
        case 5:
          fac = -(sqrt(2145./M_PI)*pow(cos(theta/2.0),7)*(-1. + 3.*cos(theta))*pow(sin(theta/2.0),3))/2.0;
          break;
        case 6:
          fac = (3.*sqrt(715./M_PI)*pow(cos(theta/2.0),8)*pow(sin(theta/2.0),4))/2.0;
          break;
        default:
          break;
      }
    } /* l==6 */
    else if ( l == 7 )
    {
      switch ( m )
      {
        case -7:
          fac = sqrt(15015./(2.0*M_PI))*pow(cos(theta/2.0),5)*pow(sin(theta/2.0),9);
          break;
        case -6:
          fac = (sqrt(2145./M_PI)*pow(cos(theta/2.0),4)*(2. + 7.*cos(theta))*pow(sin(theta/2.0),8))/2.0;
          break;
        case -5:
          fac = (sqrt(165./(2.0*M_PI))*pow(cos(theta/2.0),3)*(93. + 104.*cos(theta)
          + 91.*cos(2.*theta))*pow(sin(theta/2.0),7))/8.0;
          break;
        case -4:
          fac = (sqrt(165./(2.0*M_PI))*pow(cos(theta/2.0),2)*(140. + 285.*cos(theta)
          + 156.*cos(2.*theta) + 91.*cos(3.*theta))*pow(sin(theta/2.0),6))/16.0;
          break;
        case -3:
          fac = (sqrt(15./(2.0*M_PI))*cos(theta/2.0)*(3115. + 5456.*cos(theta) + 4268.*cos(2.*theta)
          + 2288.*cos(3.*theta) + 1001.*cos(4.*theta))*pow(sin(theta/2.0),5))/128.0;
          break;
        case -2:
          fac = (sqrt(15./M_PI)*(5220. + 9810.*cos(theta) + 7920.*cos(2.*theta) + 5445.*cos(3.*theta)
          + 2860.*cos(4.*theta) + 1001.*cos(5.*theta))*pow(sin(theta/2.0),4))/512.0;
          break;
        case -1:
          fac = (3.*sqrt(5./(2.0*M_PI))*cos(theta/2.0)*(1890. + 4130.*cos(theta) + 3080.*cos(2.*theta)
          + 2805.*cos(3.*theta) + 1430.*cos(4.*theta) + 1001.*cos(5*theta))*pow(sin(theta/2.0),3))/512.0;
          break;
        case 0:
          fac = (3.*sqrt(35./M_PI)*cos(theta)*(109. + 132.*cos(2.*theta)
          + 143.*cos(4.*theta))*pow(sin(theta),2))/512.0;
          break;
        case 1:
          fac = (3.*sqrt(5./(2.0*M_PI))*pow(cos(theta/2.0),3)*(-1890. + 4130.*cos(theta) - 3080.*cos(2.*theta)
          + 2805.*cos(3.*theta) - 1430.*cos(4.*theta) + 1001.*cos(5.*theta))*sin(theta/2.0))/512.0;
          break;
        case 2:
          fac = (sqrt(15./M_PI)*pow(cos(theta/2.0),4)*(-5220. + 9810.*cos(theta) - 7920.*cos(2.*theta)
          + 5445.*cos(3.*theta) - 2860.*cos(4.*theta) + 1001.*cos(5.*theta)))/512.0;
          break;
        case 3:
          fac = -(sqrt(15./(2.0*M_PI))*pow(cos(theta/2.0),5)*(3115. - 5456.*cos(theta) + 4268.*cos(2.*theta)
          - 2288.*cos(3.*theta) + 1001.*cos(4.*theta))*sin(theta/2.0))/128.0;
          break;
        case 4:
          fac = (sqrt(165./(2.0*M_PI))*pow(cos(theta/2.0),6)*(-140. + 285.*cos(theta) - 156.*cos(2*theta)
          + 91.*cos(3.*theta))*pow(sin(theta/2.0),2))/16.0;
          break;
        case 5:
          fac = -(sqrt(165./(2.0*M_PI))*pow(cos(theta/2.0),7)*(93. - 104.*cos(theta)
          + 91.*cos(2.*theta))*pow(sin(theta/2.0),3))/8.0;
          break;
        case 6:
          fac = (sqrt(2145./M_PI)*pow(cos(theta/2.0),8)*(-2. + 7.*cos(theta))*pow(sin(theta/2.0),4))/2.0;
          break;
        case 7:
          fac = -(sqrt(15015./(2.0*M_PI))*pow(cos(theta/2.0),9)*pow(sin(theta/2.0),5));
          break;
        default:
          break;
      }
    } /* l==7 */
    else if ( l == 8 )
    {
      switch ( m )
      {
        case -8:
          fac = sqrt(34034./M_PI)*pow(cos(theta/2.0),6)*pow(sin(theta/2.0),10);
          break;
        case -7:
          fac = sqrt(17017./(2.0*M_PI))*pow(cos(theta/2.0),5)*(1. + 4.*cos(theta))*pow(sin(theta/2.0),9);
          break;
        case -6:
          fac = sqrt(255255./M_PI)*pow(cos(theta/2.0),4)*(1. + 2.*cos(theta))
          *sin(M_PI/4.0 - theta/2.0)*sin(M_PI/4.0 + theta/2.0)*pow(sin(theta/2.0),8);
          break;
        case -5:
          fac = (sqrt(12155./(2.0*M_PI))*pow(cos(theta/2.0),3)*(19. + 42.*cos(theta)
          + 21.*cos(2.*theta) + 14.*cos(3.*theta))*pow(sin(theta/2.0),7))/8.0;
          break;
        case -4:
          fac = (sqrt(935./(2.0*M_PI))*pow(cos(theta/2.0),2)*(265. + 442.*cos(theta) + 364.*cos(2.*theta)
          + 182.*cos(3.*theta) + 91.*cos(4.*theta))*pow(sin(theta/2.0),6))/32.0;
          break;
        case -3:
          fac = (sqrt(561./(2.0*M_PI))*cos(theta/2.0)*(869. + 1660.*cos(theta) + 1300.*cos(2.*theta)
          + 910.*cos(3.*theta) + 455.*cos(4.*theta) + 182.*cos(5.*theta))*pow(sin(theta/2.0),5))/128.0;
          break;
        case -2:
          fac = (sqrt(17./M_PI)*(7626. + 14454.*cos(theta) + 12375.*cos(2.*theta) + 9295.*cos(3.*theta)
          + 6006.*cos(4.*theta) + 3003.*cos(5.*theta) + 1001.*cos(6.*theta))*pow(sin(theta/2.0),4))/512.0;
          break;
        case -1:
          fac = (sqrt(595./(2.0*M_PI))*cos(theta/2.0)*(798. + 1386.*cos(theta) + 1386.*cos(2.*theta)
          + 1001.*cos(3.*theta) + 858.*cos(4.*theta) + 429.*cos(5.*theta) + 286.*cos(6.*theta))*pow(sin(theta/2.0),3))/512.0;
          break;
        case 0:
          fac = (3.*sqrt(595./M_PI)*(210. + 385.*cos(2.*theta) + 286.*cos(4.*theta)
          + 143.*cos(6.*theta))*pow(sin(theta),2))/4096.0;
          break;
        case 1:
          fac = (sqrt(595./(2.0*M_PI))*pow(cos(theta/2.0),3)*(798. - 1386.*cos(theta) + 1386.*cos(2.*theta)
          - 1001.*cos(3.*theta) + 858.*cos(4.*theta) - 429.*cos(5.*theta) + 286.*cos(6.*theta))*sin(theta/2.0))/512.0;
          break;
        case 2:
          fac = (sqrt(17./M_PI)*pow(cos(theta/2.0),4)*(7626. - 14454.*cos(theta) + 12375.*cos(2.*theta)
          - 9295.*cos(3.*theta) + 6006.*cos(4.*theta) - 3003.*cos(5.*theta) + 1001.*cos(6.*theta)))/512.0;
          break;
        case 3:
          fac = -(sqrt(561./(2.0*M_PI))*pow(cos(theta/2.0),5)*(-869. + 1660.*cos(theta) - 1300.*cos(2.*theta)
          + 910.*cos(3.*theta) - 455.*cos(4.*theta) + 182.*cos(5.*theta))*sin(theta/2.0))/128.0;
          break;
        case 4:
          fac = (sqrt(935./(2.0*M_PI))*pow(cos(theta/2.0),6)*(265. - 442.*cos(theta) + 364.*cos(2.*theta)
          - 182.*cos(3.*theta) + 91.*cos(4.*theta))*pow(sin(theta/2.0),2))/32.0;
          break;
        case 5:
          fac = -(sqrt(12155./(2.0*M_PI))*pow(cos(theta/2.0),7)*(-19. + 42.*cos(theta) - 21.*cos(2.*theta)
          + 14.*cos(3.*theta))*pow(sin(theta/2.0),3))/8.0;
          break;
        case 6:
          fac = sqrt(255255./M_PI)*pow(cos(theta/2.0),8)*(-1. + 2.*cos(theta))*sin(M_PI/4.0 - theta/2.0)
          *sin(M_PI/4.0 + theta/2.0)*pow(sin(theta/2.0),4);
          break;
        case 7:
          fac = -(sqrt(17017./(2.0*M_PI))*pow(cos(theta/2.0),9)*(-1. + 4.*cos(theta))*pow(sin(theta/2.0),5));
          break;
        case 8:
          fac = sqrt(34034./M_PI)*pow(cos(theta/2.0),10)*pow(sin(theta/2.0),6);
          break;
        default:
          break;
      }
    } /* l==8 */
  }
    *rY = cos((double)(m)*phi) * fac;
    *iY = sin((double)(m)*phi) * fac;
}

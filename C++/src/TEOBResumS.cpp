/** salve
 * Copyright (C) 2017 Alessandro Nagar, Gregorio Carullo, Ka Wa Tsang, Philipp Fleig, Sebastiano Bernuzzi, Walter Del Pozzo
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with with program; see the file COPYING. If not, write to the
 *  Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 *  MA  02111-1307  USA
 */

#include <iostream>
#include <list>
#include <fstream>
#include <stdio.h>
#include <time.h>
#include <cmath>
#include <string>
#include <cstring>
#include <gsl/gsl_math.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_odeiv2.h>
#include <tuple>
#include <vector>
#include <complex.h>
#include <sys/stat.h>

#include "TEOBResumS.h"

using namespace::std;

static void swap_variables(double *v1, double *v2)
{
    double tmp;
    tmp = *v1;
    *v1 = *v2;
    *v2 = tmp;
}

void TEOBResumS(Waveform **hplus,       /** h+ return array                                      **/
                Waveform **hcross,      /** hx return array                                      **/
                double m1,              /** m1 (Msun)                                            **/
                double m2,              /** m2 (Msun)                                            **/
                double spin1x,          /** dimensionless s1x                                    **/
                double spin1y,          /** dimensionless s1y                                    **/
                double spin1z,          /** dimensionless s1z                                    **/
                double spin2x,          /** dimensionless s2x                                    **/
                double spin2y,          /** dimensionless s2y                                    **/
                double spin2z,          /** dimensionless s2z                                    **/
                double inclination,     /** inclination angle (rad)                              **/
                double coa_phase,       /** coalescence angle (rad)                              **/
                double f_min,           /** starting frequency (Hz)                              **/
                double dt,              /** sampling interval (s)                                **/
                double LambdaAl2,       /** l=2 (tidal deformation of body 1)/(mass of body 1)^5 **/
                double LambdaBl2,       /** l=2 (tidal deformation of body 2)/(mass of body 2)^5 **/
                double LambdaAl3,       /** l=3 (tidal deformation of body 1)/(mass of body 1)^5 **/
                double LambdaBl3,       /** l=3 (tidal deformation of body 2)/(mass of body 2)^5 **/
                double LambdaAl4,       /** l=4 (tidal deformation of body 1)/(mass of body 1)^5 **/
                double LambdaBl4,       /** l=4 (tidal deformation of body 2)/(mass of body 2)^5 **/
                double distance,        /** distance(Mpc)                                        **/
                int    lm,              /** multipole index for output                           **/
                TEOBResumFlags *flags   /** flags **/
){
    
    int i = 0;
    int grid_length = 0;
    double rLR;           /** location of the light ring **/
    double r;             /** relative separation: EOB coordinates **/ 
    double prstar;        /** radial momentum conjugate to the r*-EOB Regge-Wheeler tortoise coordinate**/
    double phi;           /** orbital phase **/
    double pphi;          /** orbital angular momentum **/
    double MOmg;          /** total orbital frequency **/
    double Omg;           /** total orbital frequency: same of MOmg. Redundacy for historical reasons **/
    double y[4];          /** vector for dynamical variables **/
    double Omg_orb;       /** "pure" orbital frequency (without the spin-orbit part) **/ 
    double MOmg_prev;     /** Orbital frequency at previous step: kept for the stop condition**/
    double t_stop;        /** stopping time **/


    double A;
    double ddotr;

    bool stop_flag, MOmgpeak_flag;
    /** check for the presence of in-plane spins **/
    if ((spin1x!=0)||(spin1y!=0)||(spin2x!=0)||(spin2y!=0))
    {
        printf("ERROR! Non-aligned spins not supported (yet)! Aborting.\n");
        exit(-1);
    }
    /** check for the multipole index **/
    if (lm > 35)
    {
        printf("Requested multipole is not available, maximum index available is 35. Aborting.\n");
        exit(-1);
    }

    if (m2 > m1)
    {
        swap_variables(&m1, &m2);
        swap_variables(&spin1x, &spin2x);
        swap_variables(&spin1y, &spin2y);
        swap_variables(&spin1z, &spin2z);
        swap_variables(&LambdaAl2, &LambdaBl2);
        swap_variables(&LambdaAl3, &LambdaBl3);
        swap_variables(&LambdaAl4, &LambdaBl4);

    }
    
    TEOBResumParams params = process_input_parameters(
                                                    m1,
                                                    m2,
                                                    spin1z,
                                                    spin2z,
                                                    f_min,
                                                    dt,
                                                    LambdaAl2,
                                                    LambdaBl2,
                                                    LambdaAl3,
                                                    LambdaBl3,
                                                    LambdaAl4,
                                                    LambdaBl4,
                                                    flags);

    double q      = params.q;
    dt            = params.dt;
    
    if (DEBUG)
    {
        printf("lambdaAl2=%f\n", params.LambdaAl2);
        printf("lambdaAl3=%f\n", params.LambdaAl3);
        printf("lambdaAl4=%f\n", params.LambdaAl4);
        printf("q=%f\n",q);
    }

    if (params.flags.tidal==1)
    {
        rLR        = AdiabLR(&params);
        params.rLR = rLR;
    }
    
    /** Defining data vectors and variables */

    std::vector<double> t_vec={};
    std::vector<double> r_vec={};
    std::vector<double> phi_vec={};
    std::vector<double> pph_vec={};
    std::vector<double> MOmg_vec={};
    std::vector<double> ddotr_vec={};
    std::vector<double> prstar_vec={};
    std::vector<double> hlm_rad_vec={};
    std::vector<double> Omg_orb_vec={};
    std::vector<double> hlm_phase_vec={};
    std::vector<vector<double> > hlm_ampl(35);
    std::vector<vector<double> > hlm_phase(35);
    
    /** Computing the initial conditions */
    vector<double> initial_data(7);
    gsl_odeiv2_system sys = {rhs, NULL , 4, &params};
    
    if (params.flags.spin==1)
    {
        sys          = {s_RHS, NULL , 4, &params};
        initial_data = s_initial(&params);
    }
    else
    {
        sys          = {rhs, NULL , 4, &params};
        initial_data = initial(&params);
    }
    

    double t     = 0.0;
    double r_LSO = 6.0;
    double t1    = 1.e15;
    double h     = dt;

    /************************************************/
    /** Setting up initial data for EOB evolution   */
    /** - initialization of the vector y[4]         */ 
    /**  y[0] = r                                   */
    /**  y[1] = phi                                 */
    /**  y[2] = pphi                                */
    /**  y[3] = prtar                               */
    /************************************************/

    y[0] = initial_data[0];
    y[1] = 0.;
    y[2] = initial_data[2];
    y[3] = initial_data[1];

    double final_mass = HealyBBHFitRemnant(spin1z, spin2z, q);
    params.Mbh = final_mass;
    
    /** Initialize ODE system solver */
    const gsl_odeiv2_step_type * T = gsl_odeiv2_step_rk8pd;
    gsl_odeiv2_step * s            = gsl_odeiv2_step_alloc(T, 4);
    gsl_odeiv2_control * c         = gsl_odeiv2_control_y_new(1.e-13, 1.e-11);
    gsl_odeiv2_evolve * e          = gsl_odeiv2_evolve_alloc(4);
    gsl_odeiv2_driver * d          = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rk8pd,dt, 1.e-13, 1.e-11);

    /************************************************/
    /*                                              */
    /* Time evolution: solution of ODEs starts here */
    /*                                              */
    /************************************************/
    
    MOmg_prev     = 0.;
    t_stop        = 0.;
    Omg           = 0.;
    Omg_orb       = 0.;
    A             = 0.;
    ddotr         = 0.;
    stop_flag     = false;
    MOmgpeak_flag = false;
    
    /* Compute and append the waveform from the initial conditions.*/
    if (std::isfinite(y[0]))
    {
        /** Waveform computation*/
        vector<gsl_complex> h_form = s_waveform(t,y,&params, Omg, Omg_orb, A, ddotr);

        if (std::isfinite(h_form[1].dat[0]) && std::isfinite(h_form[1].dat[1]))
        {
            /** Append dynamics and waveform to vectors */
            hlm_rad_vec.push_back(h_form[lm].dat[0]);
            hlm_phase_vec.push_back(h_form[lm].dat[1]);
            
            for (int k=35; k--; )
            {
                hlm_ampl[k].push_back(h_form[k].dat[0]);
                hlm_phase[k].push_back(h_form[k].dat[1]);
            }
        
            t_vec.push_back(t);
            MOmg_vec.push_back(Omg);
            r_vec.push_back(r);
            pph_vec.push_back(pphi);
            prstar_vec.push_back(prstar);
            Omg_orb_vec.push_back(Omg_orb);
            ddotr_vec.push_back(ddotr);
        }
        else stop_flag = true;
    }
    else
    {
        if (DEBUG) printf("Warning! Dynamics not well behaved (nan)\n!");
        stop_flag = true;
    }
    
    /** start the do - while loop to integrate the PDEs **/
    do
    {
        switch (params.flags.solver_scheme)
        {
            case 0:
            {
                int status = gsl_odeiv2_evolve_apply (e, c, s, &sys, &t, t1, &h, y);

                if (status != GSL_SUCCESS)
                {
                    break;
                }
                break;
            }
            case 1:
            {
                if (y[0]>r_LSO)
                {
                    int status = gsl_odeiv2_evolve_apply (e, c, s, &sys, &t, t1, &h, y);
                    if (status != GSL_SUCCESS)
                    {
                        break;
                    }
                }
                else
                {
                    double ti = t+dt;
                    int status = gsl_odeiv2_driver_apply (d, &t, ti, y);
                    if (status != GSL_SUCCESS)
                    {
                        printf ("error, return value=%d\n", status);
                        break;
                    }
                }
                break;
            }
            case 2:
            {
                double ti = t+dt;
                int status = gsl_odeiv2_driver_apply (d, &t, ti, y);
                if (status != GSL_SUCCESS)
                {
                    printf ("error, return value=%d\n", status);
                    break;
                }
                break;
            }
            default:
            {
                int status = gsl_odeiv2_evolve_apply (e, c, s, &sys, &t, t1, &h, y);
                if (status != GSL_SUCCESS)
                {
                    break;
                }
                break;
            }
        }
        
        /** Read out computation */
        r      = y[0];
        phi    = y[1];
        prstar = y[2];
        pphi   = y[3];
        
        /** Checking whether the dynamics produces NaN values
            this can happen if radius r becomes too small */
        if (std::isfinite(r))
        {
            /** Waveform computation*/
            vector<gsl_complex> h_form = s_waveform(t,y,&params, Omg, Omg_orb, A, ddotr);

            if (std::isfinite(h_form[1].dat[0]) && std::isfinite(h_form[1].dat[1]))
            {
                /** Append dynamics and waveform to vectors */
                hlm_rad_vec.push_back(h_form[lm].dat[0]);
                hlm_phase_vec.push_back(h_form[lm].dat[1]);
                
                for (int k=35; k--; )
                {
                    hlm_ampl[k].push_back(h_form[k].dat[0]);
                    hlm_phase[k].push_back(h_form[k].dat[1]);
                }
            
                t_vec.push_back(t);
                MOmg_vec.push_back(Omg);
                r_vec.push_back(r);
                phi_vec.push_back(phi);
                pph_vec.push_back(pphi);
                prstar_vec.push_back(prstar);
                Omg_orb_vec.push_back(Omg_orb);
                ddotr_vec.push_back(ddotr);
            }
            else stop_flag = true;
        }
        else
        {
            if (DEBUG) printf("Warning! Dynamics not well behaved (nan)\n!");
            stop_flag = true;
            break;
        }
        /** Breaking the computation. Find the peak of the Omg_orb curve (the "pure" orbital frequency
            without the spin-orbit contribution) and continue the evolution for another 5M to
            avoid interpolation problems later.
            For the non-spinning case Omg_orb is precisely the orbital frequency */
	
        if (params.flags.spin==1)
        {
            MOmg = Omg_orb;
        }
        else
        {
            MOmg = Omg;
            
        }	
        if (MOmgpeak_flag==false)
        {
            if (MOmg < MOmg_prev)
            {
                MOmgpeak_flag = true;
                t_stop        = t + 5.*dt; 
            }
            else
            {
                MOmg_prev = MOmg;
            }
        }
        else
        {
            if (t >= t_stop)
            {
                stop_flag = true;
            }
        }
	
	/***************************************************/
	/** Quick hack: stop the waveform during inspiral and
            compute Qomg. Solver 2 is better in this case, to
            avoid uncertainty coming from interpolation*/
	
	/*if (r<=10)
	 {
                stop_flag = true;
		}*/
	
    }while (stop_flag == false);
    gsl_odeiv2_evolve_free (e);
    gsl_odeiv2_control_free (c);
    gsl_odeiv2_step_free (s);
    gsl_odeiv2_driver_free (d);


    /*********************************************************
     IMPORTANT STEP HERE: with solver 2, this waveform is OK
     for Qomg computation. Output of the pure RK evolution.
    **********************************************************/

#if (DEBUG) 
 	char   outputd[256]   = "dyn.dat";	
        std::FILE* dynamics   = std::fopen(outputd, "w");
        int j                 = 0;
        int Nd                 = hlm_ampl[1].size();
        
        for (j=1;j<Nd;j++)  // For some reason C++ prints phi_vec with an offset.
        {
	        std::fprintf(dynamics, "%.16e\t%.16e\t%.16e\t%.16e\t%.16e\t%.16e\t%.16e\t%.16e\n", t_vec[j], r_vec[j], phi_vec[j-1], pph_vec[j], MOmg_vec[j], ddotr_vec[j],prstar_vec[j],Omg_orb_vec[j]);
        }
        std::fclose(dynamics);
#endif

#if (DEBUG) 
        char   outputr[256]   = "h22_inspl.dat";	
        std::FILE* waveform_preint   = std::fopen(outputr, "w");
        //int j                 = 0;
        int Ntmp                 = hlm_ampl[1].size();
        
        for (j=0;j<Ntmp;j++)
        {
            std::fprintf(waveform_preint, "%20.12f\t%.16e\t%.16e\n", t_vec[j], hlm_ampl[1][j], hlm_phase[1][j]);
        }
        std::fclose(waveform_preint);
#endif

/* Check ALL inspiral waveforms */
#if (DEBUG)
	char   outputins[256]      = "Waveform_insp_ALL_modes.dat";
	std::FILE* waveform_insp   = std::fopen(outputins, "w");
	int Nins                   = hlm_ampl[1].size();
	
	for(int k=0; k< 35; k++)        
	{
	        for (j=0; j<Nins; j++)
	        {

			if( fabs(hlm_ampl[k][j]) != 0.0 ) std::fprintf(waveform_insp, "%20.12f\t%.16e\t%.16e\n", t_vec[j], hlm_ampl[k][j], hlm_phase[k][j]);
        	}
	}
        std::fclose(waveform_insp);

#endif
    
#if (BNS_OUTPUTMODES_EXIT)
	/* MODIFS FOR USED IN RAPID PE OF BNS:
	   output the modes and exit. */
	char   outputr[256]   = "hlm_insp.dat";
	std::FILE* waveform_preint   = std::fopen(outputr, "w");
	int j                 = 0;
	int lm_index          = 0;
	int Nw                 = hlm_ampl[1].size();
	
	for (j=0;j<Nw;j++)
	  {
	    std::fprintf(waveform_preint, "%20.12f\t",t_vec[j]);
	    for (lm_index=0;lm_index<35;lm_index++)
	      {
		std::fprintf(waveform_preint, "%20.12f\t %20.12f\t ", hlm_ampl[lm_index][j], hlm_phase[lm_index][j]);
	      }
	    std::fprintf(waveform_preint, "\n");
	  }
	std::fclose(waveform_preint);
	/* BYE BYE */
	exit(0);      
#endif

    /** Interpolate quantities on a grid of fixed spacing dt */
    grid_length = (int)((t_vec.back()-t_vec[0])/dt + 1);
    vector<double> t_vecg(grid_length);
    i  = 0;
    double ti = 0.;
    for (ti = t_vec[0]; ti < t_vec.back(); ti += dt)
    {
        t_vecg[i] = ti;
        i++;
	
    }
  
    vector<double> hlm_phase_vecg = interp_grid(t_vec,hlm_phase_vec,dt);
    vector<double> hlm_rad_vecg   = interp_grid(t_vec,hlm_rad_vec,dt);
    vector<double> r_vecg         = r_vec;
    vector<double> MOmg_vecg      = MOmg_vec;
    vector<double> pph_vecg       = pph_vec;
    vector<double> prstar_vecg    = prstar_vec;
    vector<double> ddotr_vecg     = ddotr_vec; 
    vector<double> OmgOrb_vecg    = Omg_orb_vec;
    if (params.flags.tidal == 0)
    {
        r_vecg         = interp_grid(t_vec, r_vec,dt);
        MOmg_vecg      = interp_grid(t_vec, MOmg_vec,dt);
        pph_vecg       = interp_grid(t_vec, pph_vec,dt);
        prstar_vecg    = interp_grid(t_vec, prstar_vec,dt);
        ddotr_vecg     = interp_grid(t_vec, ddotr_vec,dt);
        OmgOrb_vecg    = interp_grid(t_vec, Omg_orb_vec,dt);
    }
    std::vector<vector<double> > hlm_ampl_g(35);
    std::vector<vector<double> > hlm_phase_g(35);
    
    for (int k=35; k--; )
    {
        vector<double> amplitude = hlm_ampl[k];
        vector<double> phase     = hlm_phase[k];
        hlm_ampl_g[k]            = interp_grid(t_vec,amplitude,dt);
        hlm_phase_g[k]           = interp_grid(t_vec,phase,dt);
    }
    
//    /** NQCs corrections */
//    if (DEBUG)
//    {
//        char   outputr[256]   = "waveform_preNQC.dat";
//        std::FILE* waveform_preNQC   = std::fopen(outputr, "w");
//        int j                 = 0;
//        int N                 = hlm_ampl_g[1].size();
//
//        for (j=0;j<N;j++)
//        {
//            std::fprintf(waveform_preNQC, "%20.12f\t%20.12f\t%20.12f\n", t_vecg[j], hlm_ampl_g[1][j], hlm_phase_g[1][j]);
//        }
//        std::fclose(waveform_preNQC);
//    }

/* Counting the modes with NON-zero GW wave amplitude */
#if(DEBUG)
	printf("\nModes with NON-zero Amplitudes:\n");
		int count= 0;
		int nonzero= 0;
		for( int l = 2; l<=8; l++){
			for(int m=1; m<= l; m++){
				if( fabs(hlm_ampl[count][3]) !=0.0 )	{printf("{%d, %d}, ", l, m); nonzero++; }
				count++;
			}
		}
	printf("\nNumber of NON-zero modes:%d\n\n", nonzero); //SARP
#endif
    
    if (params.flags.tidal==0 && params.flags.spin==1)
    {
        
        vector<vector<gsl_complex> > nqc = find_a1a2a3(t_vecg,r_vecg,MOmg_vecg,pph_vecg,prstar_vecg,hlm_phase_g,OmgOrb_vecg,hlm_ampl_g,ddotr_vecg,&params);
		
        for (int k=35; k--; )
        {
            if (k==1)
            {
                for (int i=0; i<grid_length; i++ )
                {
                    hlm_ampl_g[k][i]  = hlm_ampl_g[k][i]  * nqc[k][i].dat[0];
                    hlm_phase_g[k][i] = hlm_phase_g[k][i] - nqc[k][i].dat[1];
                }
            }
        }
    }

//    if (DEBUG)
//    {
//        char   outputr[256]   = "waveform_postNQC.dat";
//        std::FILE* waveform_postNQC   = std::fopen(outputr, "w");
//        int j                 = 0;
//        int N                 = hlm_ampl_g[1].size();
//        
//        for (j=0;j<N;j++)
//        {
//        std::fprintf(waveform_postNQC, "%20.12f\t%.16e\t%.16e\n", t_vecg[j], hlm_ampl_g[1][j], hlm_phase_g[1][j]);
//        }
//        std::fclose(waveform_postNQC);
//    }
    /** Define a time vector for each multipole
        These will be cut by the ringdown, where
        each multipole has its own starting time */
    
    vector<vector<double> > t_g(35);
    for (int k=35; k--; )
    {
        t_g[k] = t_vecg;
    }
    
    /** Ringdown attachment */
    if (params.flags.tidal==0)
    {
        ringdown(params, t_g, OmgOrb_vecg, hlm_ampl_g, hlm_phase_g);
    }
    /** All multipoles will now have size N+Nringdown */
    /** Multipole for which no ringdown model is available will be filled with 0s */
    /** We pick the index 1 since it is the 22 mode and it is always computed */
    
    int N = hlm_ampl_g[1].size();

    /** Allocate hplus and hcross */
    Waveform *hplus_out = (Waveform *)malloc(sizeof(Waveform));
    if (hplus_out == NULL)
    {
        printf("ERROR allocating hplus.\n");
        exit(-1);
    }
    hplus_out->data = (double *)malloc(N*sizeof(double));
    if (hplus_out->data == NULL)
    {
        printf("ERROR allocating hplus->data.\n");
        exit(-1);
    }
    hplus_out->length = N;
    Waveform *hcross_out = (Waveform *)malloc(sizeof(Waveform));
    if (hcross_out == NULL)
    {
        printf("ERROR allocating hcross.\n");
        exit(-1);
    }
    hcross_out->data = (double *)malloc(N*sizeof(double));
    if (hcross_out->data == NULL)
    {
        printf("ERROR allocating hcross->data.\n");
        exit(-1);
    }
    hcross_out->length = N;
    
    /** Set them to zero initially */
    
    memset(hplus_out->data, 0, N*sizeof(double));
    memset(hcross_out->data, 0, N*sizeof(double));
    
    /** Spherical harmonics projection **/
    /** construct hplus and hcross **/
    /** h22 = 1/R * (nu*M)*G/c^2 h_code_output */

    double mtot_m = 1.;
    double amplitude_prefactor = 1.;    
    if (!(params.flags.geometric_units))
    {
      mtot_m = (m1+m2)*MSUN_M;
      amplitude_prefactor = params.nu*mtot_m/(distance*MPC_M);
    } 

    if (params.flags.multipoles == 1)
    {
        for (i=0; i<N; i++)
        {
            hplus_out->data[i]   = hlm_ampl_g[lm][i];
            hcross_out->data[i]  = hlm_phase_g[lm][i];
        }

    }
    else
    {
        for (int k=35; k--; )
        {
            if (k==1)
            {
//                double Y_real, Y_imag;
                double Y_real_minus, Y_imag_minus;
                double Y_real_plus, Y_imag_plus;
//                spinsphericalharm(&Y_real, &Y_imag, -2, L[k], M[k], coa_phase, inclination);
                // for debugging
		//printf("standard: %f %f\n",Y_real,Y_imag);
                //LALSpinWeightedSphericalHarmonic(&Y_real, &Y_imag, -2, L[k], M[k], inclination, coa_phase);
                //printf("LAL: %f %f\n",Y_real,Y_imag);
                /* Computing spherical harmonics with +m an -m */
                spinsphericalharm(&Y_real_plus,  &Y_imag_plus,  -2, L[k],  M[k], coa_phase, inclination);
                spinsphericalharm(&Y_real_minus, &Y_imag_minus, -2, L[k], -M[k], coa_phase, inclination);
		
                /** there is a MINUS SIGN in the phase h = A exp(-i phase) **/
                
                for (i=0; i<N; i++)
                {
                    double Aki = hlm_ampl_g[k][i]*amplitude_prefactor;
                    double cosPhi = cos(hlm_phase_g[k][i]);
                    double sinPhi = -sin(hlm_phase_g[k][i]);
		    /* h+ and hx
		       This stems from

                       h+ - i h_x = A_lm(exp(-iPhi) -2Y_lm + exp(iPhi) -2Yl-m

                       where here Phi is the GW phase and is positive.
                       Once separated in real and imaginary part, one obtains
                       the following expressions for h+ and hx */
		    
		    hplus_out->data[i]  += Aki*(cosPhi*(Y_real_plus + Y_real_minus) + sinPhi*( -Y_imag_plus + Y_imag_minus)); 
		    hcross_out->data[i] -= Aki*(cosPhi*(Y_imag_plus + Y_imag_minus) + sinPhi*(  Y_real_plus - Y_real_minus));

		    
                }
            }
        }
    }
    *hplus = hplus_out;
    *hcross= hcross_out;
}
